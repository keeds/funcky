(ns funcky.core)

(comment

  1

  (type 1)

  "a"

  (type "a")

  [1 2 3]

  (type [1 2 3])

  (= [1 2 3] [1 2 3])

  (type (range 10))

  (= 1 true)

  (1 2 3)

  '(1 2 3)

  (type '(1 2 3))

  (conj [1 2 3] 4)

  (conj '(1 2 3) 4)

  (cons 4 [1 2 3])

  (cons 4 '(1 2 3))

  (type (cons 4 [ 1 2 3]))

  (type (cons 4 '(1 2 3)))

  ;; maps
  {"a" 1 "b" 2}

  (type {"a" 1 "b" 3})

  (get {"a" 1 "b" 2} "a")

  (get {"a" 1 "b" 2} "z")

  (get {"a" 1 "b" 2} "z" 0)

  :a

  (type :a)

  ::a

  :wanda/dress

  {:a 1 :b 2}

  (:a {:a 1 :b 2})

  (assoc {:a 1 :b 2} :a 3)

  (def a {:a 1 :b 2})

  a

  (= (assoc a :a 3) (assoc a :a 3))

  a

  (inc 1)

  (range 10)

  (map inc (range 10))

  (apply + (range 10))

  (filter even? (range 10))

  (reduce (fn [x y] (* x y)) 1 [1 2 3 4 5])

  ;; Threading

  (range 10)

  (filter even? (range 10))

  (map inc (filter even? (range 10)))

  (reduce (fn [x y] (* x y)) (map inc (filter even? (range 10))))

  (->> (range 10)
       (filter even?)
       (map inc)
       (reduce (fn [x y] (* x y))))

  (def a [{:id 1 :value 1 :cust 999}
          {:id 2 :value 23 :cust 100}
          {:id 3 :value 99 :cust 999}])

  a

  (map :id a)

  (map + (range 10) (range 10))

  (map :value a)

  (filter (fn [x] (= (:cust x) 999)) a)

  (filter #(= (:cust %) 999) a)

  (->> a
       (filter #(= (:cust %) 999))
       (map :value)
       (reduce +))

  ((fn [m id]
      (->> m
           (filter #(= (:cust %) id))
           (map :value)
           (reduce +)))
   a 999)

  (defn customer-total
    [m id]
    (->> m
         (filter #(= (:cust %) id))
         (map :value)
         (reduce +)))

  (customer-total a 999)

  (customer-total 999 a)

  (defn customer-total
    "calculate total"
    [m id]
    {:pre [(vector? m)
           (number? id)]}
    (->> m
         (filter #(= (:cust %) id))
         (map :value)
         (reduce +)))

  (customer-total a a)

  (customer-total 999 a)


  ;; Sequences
  (first (range 10))

  (rest (range 10))

  (seq [1 2 3])

  )
